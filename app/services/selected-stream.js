import Service from '@ember/service';

export default Service.extend({
  stream: "",
   init() {
    this._super(...arguments);
  },

  set_stream(s) {
    this.set('stream', s);
  }
});
