import Component from '@ember/component';
import { inject as service } from '@ember/service';
//import { observer } from '@ember/object';

export default Component.extend({
  selected: service('selected-stream'),
  isPaused: null,
  disabled: true,
  oldSource: "",
  set_paused(val){
    this.set('isPaused', val);
    this.set('disabled', false);
    console.log(val);
  },
  
  actions: {
    clickedPlayPause: function() {
      if (this.get("isPaused") ) {
        //play
        this.$("audio").trigger("play");
      } else {
        //pause
        this.$("audio").trigger("pause");
      }
    }
  },
  //when this is added to the DOM
  didInsertElement(){
    //add some listeners to the <audio>
    var self = this;
    this.$("audio").on("pause", function(){
      self.set_paused(true);
    });
    this.$("audio").on("playing", function(){
      self.set_paused(false);
    });
  }
});
