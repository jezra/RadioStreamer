import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  selected: service('selected-stream'),
  actions: {
    doSelect: function() {
      this.get('selected').set_stream(this.stream);
      //trigger 'play' on the Audio element in a few microts
      setTimeout("$('#stream-player-audio').trigger('play')", 50);
    }
  }
});
