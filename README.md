# RadioStreamer

RadioStreamer provides a basic UI for easily selecting and listening to streams
from the Public Radio Stations that I listen to. RadioStreamer is also
 an absurdly overcomplicated HTML5 app-type thingy that
could easily be replaced with a few dozen lines of javascript.

## Prerequisites

You will need the following things properly installed on your computer.

* [Node.js](https://nodejs.org/) (with NPM)
* [Ember CLI](https://ember-cli.com/)


## Installation

* `git clone <repository-url>` this repository
* `cd radio-streamer`
* `npm install`

## Running 

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).

